### [6.5.1] - 2019-08-07
### Added
- tvOS support

### [6.5.0] - 2019-08-07
### Added
- New ad methods

## [6.0.7] - 2019-03-04
### Add
- CFBundleShortString is now hardcoded to avoid problems
### Improved
- Same versioning as the library pod

## [6.0.3] - 2019-02-13
### Improved
- Ad title now shows creative id

## [6.0.2] - 2018-11-22
### Fix
- Position reported properly
- Duration reported is now Ad, not Slot

## [6.0.2] - 2018-11-22
### Fix
- Position reported properly
- Duration reported is now Ad, not Slot

## [6.0.1] - 2018-04-19
### Fix
 - Fixed playhead not reseting after an Ad

## [6.0.0] - 2018-03-27
### Release
