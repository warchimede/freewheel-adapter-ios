//
//  FreeWheelAdManager.h
//  PrerollDemo
//
//  Created by Shitong (Stone) Wang on 3/23/16.
//  Copyright © 2016 Shitong (Stone) Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AdManager/AdManager.h>

//@import AdManager;

@interface FreeWheelAdManager : NSObject

+ (id<FWAdManager>)sharedAdManager;

@end
