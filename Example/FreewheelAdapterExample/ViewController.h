//
//  ViewController.h
//  PrerollDemo
//
//  Created by Shitong (Stone) Wang on 4/21/16.
//  Copyright © 2016 Shitong (Stone) Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController


@property (nonatomic, strong) NSMutableArray *prerollSlots;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVPlayerLayer *layer;


@property (nonatomic, retain) IBOutlet UIView *videoDisplayBase;
@property (nonatomic, retain) IBOutlet UIButton *restartButton;

- (IBAction)onRestartButtonClick:(id)sender;

@end

