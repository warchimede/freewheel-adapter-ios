//
//  ViewController.m
//  PrerollDemo
//
//  Created by Shitong (Stone) Wang on 3/23/16.
//  Copyright © 2016 Shitong (Stone) Wang. All rights reserved.
//

#import "ViewController.h"
#import <YouboraConfigUtils/YouboraConfigUtils.h>
#import "FreeWheelAdManager.h"

@import YouboraFreewheelAdapter;
@import YouboraConfigUtils;

#define kDemoPlayerProfile @"96749:global-cocoa"
#define kDemoVideoAssetId @"DemoVideoGroup.01"
#define kDemoSiteSectionId @"DemoSiteGroup.01"

@interface ViewController()

@property (nonatomic, strong) id<FWContext> adContext;
@property (nonatomic, strong) FWRequestConfiguration *adRequestConfig;

@property (nonatomic, strong) YBPlugin * youboraPlugin;
@property (nonatomic, strong) YBFreewheelAdapter * adapter;

@end

@implementation ViewController

@synthesize player;
@synthesize layer;
@synthesize adContext = _adContext;
@synthesize prerollSlots = _prerollSlots;

- (void)viewDidLoad {
	[super viewDidLoad];

	[self startDemo];
}

- (void)startDemo {
    [YBLog setDebugLevel:YBLogLevelVerbose];
    YBOptions *options = [YouboraConfigManager getOptions];
    options.offline = NO;
    options.accountCode = @"powerdev";
    self.youboraPlugin = [[YBPlugin alloc] initWithOptions:options];
    [self.youboraPlugin fireInit];
	// Configure the ad context and make a request
	_adContext = [[FreeWheelAdManager sharedAdManager] newContext];
    YBFreewheelAdapter *adapter = [[YBFreewheelAdapter alloc] initWithPlayer:_adContext];
    [self.youboraPlugin setAdsAdapter:adapter];

	// Consult your FreeWheel Solution Engineer for the values to use
	self.adRequestConfig = [[FWRequestConfiguration alloc] initWithServerURL:@"http://demo.v.fwmrm.net" playerProfile:kDemoPlayerProfile];
	self.adRequestConfig.videoAssetConfiguration = [[FWVideoAssetConfiguration alloc] initWithVideoAssetId:kDemoVideoAssetId idType:FWIdTypeCustom duration:100 durationType:FWVideoAssetDurationTypeExact autoPlayType:FWVideoAssetAutoPlayTypeAttended];
	self.adRequestConfig.siteSectionConfiguration = [[FWSiteSectionConfiguration alloc] initWithSiteSectionId:kDemoSiteSectionId idType:FWIdTypeCustom];

	// Optionally, request ads for slots you define. Not necessary if you have Commercial Break Pattern setup in MRM.
	[self.adRequestConfig addSlotConfiguration:[[FWTemporalSlotConfiguration alloc] initWithCustomId:@"Preroll1" adUnit:FWAdUnitPreroll timePosition:0]];
    //[self.adRequestConfig addSlotConfiguration:[[FWTemporalSlotConfiguration alloc] initWithCustomId:@"Midroll1" adUnit:FWAdUnitMidroll timePosition:5]];

	// The video display base needs to be set so that AdManager knows where to render temporal ads.
	[self.adContext setVideoDisplayBase:self.videoDisplayBase]; 

	// Listen to important notifications
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRequestComplete:) name:FWRequestCompleteNotification object:self.adContext];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSlotEnded:) name:FWSlotEndedNotification object:self.adContext];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onContentPauseRequest:) name:FWContentPauseRequestNotification object:self.adContext];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onContentResumeRequest:) name:FWContentResumeRequestNotification object:self.adContext];

	[self.adContext submitRequestWithConfiguration:self.adRequestConfig timeout:10];
}

- (void)onRequestComplete:(NSNotification *)notification {
	_prerollSlots = [[NSMutableArray alloc] initWithArray:[self.adContext getSlotsByTimePositionClass:FWTimePositionClassPreroll]];
	[self playNextPreroll];
}

- (void)playNextPreroll {
	if (self.prerollSlots.count > 0) {
		id<FWSlot> slot = [self.prerollSlots objectAtIndex:0];
		[slot play];
		[self.prerollSlots removeObjectAtIndex:0];
	}
	else {
		self.prerollSlots = nil;
		[self playContentVideo];
	}
}

- (void)onSlotEnded:(NSNotification *)notification {
	NSString *slotCustomId = [notification.userInfo objectForKey:FWInfoKeySlotCustomId];
	id<FWSlot> slot = [self.adContext getSlotByCustomId:slotCustomId];
	if ([slot timePositionClass] == FWTimePositionClassPreroll) {
		[self playNextPreroll];
	}
}

- (void)playContentVideo {
	// Initialize the player
	NSURL *url = [NSURL URLWithString:@"http://vi.freewheel.tv/static/content/freewheel.m3u8"];
	AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:url];
	self.player = [AVPlayer playerWithPlayerItem:playerItem];

	self.layer = [AVPlayerLayer playerLayerWithPlayer:self.player];
	[self.layer setFrame:self.videoDisplayBase.bounds];
	[self.videoDisplayBase.layer addSublayer:self.layer];
	[self.player play];

	[self.adContext setVideoState:FWVideoStatePlaying];
}

- (void)onContentPauseRequest:(NSNotification *)notification {
	[self.player pause];
	[self.adContext setVideoState:FWVideoStatePaused];
}

- (void)onContentResumeRequest:(NSNotification *)notification {
	[self.player play];
	[self.adContext setVideoState:FWVideoStatePlaying];
}

- (IBAction)onRestartButtonClick:(id)sender {
	// by releasing the adContext(non-ARC) / setting the adContext to nil(ARC), all running ads are stopped immediately.
	self.adContext = nil;
	[self.layer removeFromSuperlayer];
    [self.youboraPlugin removeAdsAdapter];
	self.player = nil;
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[self startDemo];
}

- (IBAction)youboraConfigButtonClicked:(id)sender {
    UIViewController * vc = [YouboraConfigViewController new];
    [[self navigationController] pushViewController:vc animated:YES];
}


@end
