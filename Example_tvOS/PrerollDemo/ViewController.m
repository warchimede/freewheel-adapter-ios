//
//  ViewController.m
//  PrerollDemo
//
//  Created by Shitong (Stone) Wang on 3/23/16.
//  Copyright © 2016 Shitong (Stone) Wang. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "FreeWheelAdManager.h"

@import YouboraFreewheelAdapter;

#define kDemoPlayerProfile @"96749:global-cocoa"
#define kDemoVideoAssetId @"DemoVideoGroup.01"
#define kDemoSiteSectionId @"DemoSiteGroup.01"

#define kDemoServerUrl @"http://demo.v.fwmrm.net"

@interface ViewController () {
	id<FWContext> adContext;
	id<FWSlot> playingSlot;
	NSMutableArray *prerollSlots;
	IBOutlet UIView *videoDisplayBase;
	IBOutlet UITextView *console;
    
    YBPlugin * youboraPlugin;
    YBFreewheelAdapter * adapter;
}

@property (nonatomic, retain) AVPlayer *player;

- (IBAction)onPlayPauseButtonClick:(id)sender;
- (IBAction)onRestartButtonClick:(id)sender;

@end

@implementation ViewController

@synthesize player;

- (void)viewDidLoad {
	[super viewDidLoad];

	[self startDemo];
}

- (void)startDemo {
    
    [YBLog setDebugLevel:YBLogLevelVerbose];
    YBOptions *options = [YBOptions new];
    options.offline = NO;
    options.accountCode = @"powerdev";
    youboraPlugin = [[YBPlugin alloc] initWithOptions:options];
    [youboraPlugin fireInit];
    
	// Configure the ad context and make a request
	adContext = [[FreeWheelAdManager sharedAdManager] newContext];
    YBFreewheelAdapter *adapter = [[YBFreewheelAdapter alloc] initWithPlayer: adContext];
    [youboraPlugin setAdsAdapter:adapter];
	FWRequestConfiguration *adRequestConfig = [[FWRequestConfiguration alloc] initWithServerURL:kDemoServerUrl playerProfile:kDemoPlayerProfile playerDimensions:CGSizeMake(300, 300)];
	FWVideoAssetConfiguration *videoAssetConfig = [[FWVideoAssetConfiguration alloc] initWithVideoAssetId:kDemoVideoAssetId idType:FWIdTypeCustom duration:100 durationType:FWVideoAssetDurationTypeExact autoPlayType:FWVideoAssetAutoPlayTypeAttended];
	FWSiteSectionConfiguration *siteSectionConfig = [[FWSiteSectionConfiguration alloc] initWithSiteSectionId:kDemoSiteSectionId idType:FWIdTypeCustom];
	adRequestConfig.videoAssetConfiguration = videoAssetConfig;
	adRequestConfig.siteSectionConfiguration = siteSectionConfig;
	// Optionally, request ads for slots you define. Not necessary if you have Commercial Break Pattern setup in MRM.
	[adRequestConfig addSlotConfiguration:[[FWTemporalSlotConfiguration alloc] initWithCustomId:@"Preroll1" adUnit:FWAdUnitPreroll timePosition:0]];
	[adContext setVideoDisplayBase:videoDisplayBase];

	// Listen to important notifications
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRequestComplete:) name:FWRequestCompleteNotification object:adContext];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSlotStarted:) name:FWSlotStartedNotification object:adContext];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSlotEnded:) name:FWSlotEndedNotification object:adContext];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onContentPauseRequest:) name:FWContentPauseRequestNotification object:adContext];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onContentResumeRequest:) name:FWContentResumeRequestNotification object:adContext];

	[adContext submitRequestWithConfiguration:adRequestConfig timeout:10];
}

- (void)onRequestComplete:(NSNotification *)notification {
	prerollSlots = [[NSMutableArray alloc] initWithArray:[adContext getSlotsByTimePositionClass:FWTimePositionClassPreroll]];
	[self logToConsole:[NSString stringWithFormat:@"Request completed. Number of prerolls: %tu.", prerollSlots.count]];
	[self playNextPreroll];
}

- (void)playNextPreroll {
	if (prerollSlots.count > 0) {
		id<FWSlot> slot = [prerollSlots objectAtIndex:0];
		[prerollSlots removeObjectAtIndex:0];
		[slot play];
		[self logToConsole:@"Preroll slot started."];
	}
	else {
		[prerollSlots release];
		[self playContentVideo];
	}
}

- (void)onSlotStarted:(NSNotification *)notification {
	NSString *slotCustomId = [notification.userInfo objectForKey:FWInfoKeySlotCustomId];
	playingSlot = [adContext getSlotByCustomId:slotCustomId];
}

- (void)onSlotEnded:(NSNotification *)notification {
	NSString *slotCustomId = [notification.userInfo objectForKey:FWInfoKeySlotCustomId];
	id<FWSlot> slot = [adContext getSlotByCustomId:slotCustomId];
	playingSlot = nil;
	if ([slot timePositionClass] == FWTimePositionClassPreroll) {
		[self logToConsole:@"Preroll slot ended."];
		[self playNextPreroll];
	}
}

- (void)playContentVideo {
	// Initialize the player
	NSURL *url = [NSURL URLWithString:@"http://vi.freewheel.tv/static/content/freewheel.m3u8"];
	AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:url];
	self.player = [AVPlayer playerWithPlayerItem:playerItem];

	AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
	[playerLayer setFrame:videoDisplayBase.bounds];
	[videoDisplayBase.layer addSublayer:playerLayer];
	[self.player play];
	[self logToConsole:@"No more preroll slots to play. Start content video."];

	[adContext setVideoState:FWVideoStatePlaying];
}

- (void)onContentPauseRequest:(NSNotification *)notification {
	[self.player pause];
	[adContext setVideoState:FWVideoStatePaused];
}

- (void)onContentResumeRequest:(NSNotification *)notification {
	[self.player play];
	[adContext setVideoState:FWVideoStatePlaying];
}

- (IBAction)onPlayPauseButtonClick:(id)sender {
	UIButton *button = (UIButton *)sender;
	if ([button.titleLabel.text isEqualToString:@"Pause"]) {
		[button setTitle:@"Play" forState:UIControlStateNormal];
		[adContext requestTimelinePause];
	}
	else {
		[button setTitle:@"Pause" forState:UIControlStateNormal];
		[adContext requestTimelineResume];
	}
}

- (IBAction)onRestartButtonClick:(id)sender {
	[adContext release];
	adContext = nil;
	if (videoDisplayBase.layer.sublayers.count > 0) {
		[videoDisplayBase.layer.sublayers[0] removeFromSuperlayer];
	}
	[player release];
	player = nil;
	console.text = @"";

	[self startDemo];
}

- (void)logToConsole:(NSString *)message {
	console.text = [NSString stringWithFormat:@"%@\n\n> %@", console.text, message];
}

@end
